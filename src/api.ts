import axios from 'axios';
import { GetRepositoryClosedIssuesRequest, ConfigJson } from './types';
import config from './config.json';

export const FindRepositoriesByText = (textFilter: string) => (
    axios({
        method: 'GET',
        url: `https://api.github.com/search/repositories?q=${textFilter}`
    })
) 

export const GetRepositoryClosedIssues = ({user, repoName}: GetRepositoryClosedIssuesRequest) => (
    axios({
        method: 'GET',
        // url: `https://api.github.com/repos/${user}/${repoName}/issues?access_token=${GetGitHubToken()}&state=closed`
        url: `https://api.github.com/search/issues?q=repo:${user}/${repoName}+type:issue+state:closed&per_page=1`
    })
)

export const GetGitHubToken = (): string => {
    return (config as ConfigJson).githubToken;
}