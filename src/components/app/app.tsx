import React, { useEffect } from 'react';
import * as Api from '../../api';
import { useGlobal } from '../../store/store';
import { GetRepositoriesByFilterResponse, RepositoryItemInterface, RequestStatus } from '../../types';
import { AxiosResponse } from 'axios';
import RepositoryItem from '../../components/repositoryItem/repositoryItem';
import './app.css';

const AppMain = () => {
    const [globalState, globalStateActions] = useGlobal();

    const handleChangeTextFilter = (event: React.ChangeEvent<HTMLInputElement>) =>
        globalStateActions.setTextFilter(event.target.value);

    const handleSearchButton = () => {
        if (globalState.textFilter === '') {
            globalStateActions.clearRepositoryList();
            globalStateActions.setRepositoriesCount(0);
        }
        else {
            Api.FindRepositoriesByText(globalState.textFilter)
                .then(({data}: AxiosResponse<GetRepositoriesByFilterResponse>) => {
                    const result = data.items.map(item => ({
                        ownerName: item.owner.login,
                        repoName: item.name,
                        closedIssues: 0,
                        status: RequestStatus.ready
                    })) as RepositoryItemInterface[];

                    globalStateActions.setRepositoryList(result);
                    globalStateActions.setRepositoriesCount(data.total_count);
                })
                .catch(err => console.dir(err));
        }
    }

    useEffect(() => {
        const data = globalState.repositories;
        let changed = false;

        if (data.length) {
            data
                .forEach((item, index) => {
                    if (item.status === RequestStatus.ready) {
                        Api.GetRepositoryClosedIssues({ user: item.ownerName, repoName: item.repoName })
                            .then((res: AxiosResponse<GetRepositoriesByFilterResponse>) => {
                                const newRepositoriesList = [...globalState.repositories];
    
                                newRepositoriesList[index].closedIssues = res.data.total_count;
                                newRepositoriesList[index].status = RequestStatus.done;
    
                                globalStateActions.setRepositoryList(newRepositoriesList);
                            });

                        item.status = RequestStatus.inProgress;
                        changed = true;
                    }
                })

            if (changed) {
                globalStateActions.setRepositoryList(data);
            }
        }
    })

    return (
        <div>
            <div className="control">
                <input
                    type="text"
                    placeholder="Search text"
                    onChange={handleChangeTextFilter}
                    value={globalState.textFilter} />
                <button
                    onClick={handleSearchButton}>Find</button>
                <span className="total-count">{globalState.totalCount} total</span>
            </div>
            <div className="data-result">
                {
                    globalState.repositories.length > 0
                        ? globalState.repositories.map((item: RepositoryItemInterface) => {
                            const {ownerName, repoName, closedIssues} = item;

                            return (
                                <RepositoryItem
                                    key={`${ownerName}${repoName}`}
                                    ownerName={ownerName}
                                    repoName={repoName}
                                    closedIssues={closedIssues} />
                            )
                        })
                        : <div>no data by this filter</div>
                }
            </div>
        </div>
    );
}

export default AppMain;
