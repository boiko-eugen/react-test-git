import React from 'react';
import './repositoryItem.css';

interface RepositoryItemComponentInterface {
    ownerName: string;
    repoName: string;
    closedIssues: number;
}

const RepositoryItem = ({ownerName, repoName, closedIssues}: RepositoryItemComponentInterface) => {
    return (
        <div className="repository-item">
            <span>{ownerName}</span>
            <span>{repoName}</span>
            <span>{closedIssues}</span>
        </div>
    )
}

export default RepositoryItem;
