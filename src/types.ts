export interface GetRepositoriesByFilterResponse {
    incomplete_results: boolean;
    items: RepositoryItemResponseInterface[];
    total_count: number;
}

export interface RepositoryItemResponseInterface {
    owner: {
        login: string
    };
    name: string;
}

export interface RepositoryItemInterface {
    ownerName: string;
    repoName: string;
    closedIssues: number;
    status: RequestStatus;
}

export interface GetRepositoryClosedIssuesRequest {
    user: string;
    repoName: string;
}

export interface ConfigJson {
    githubToken: string;
}

export enum RequestStatus {
    ready,
    inProgress,
    done
}