import { Store } from "use-global-hook"
import { RepositoryItemInterface } from "../types";

export interface RepositoryState {
    repositories: RepositoryItemInterface[];
    totalCount: number;
}

export const initialState: RepositoryState = {
    repositories: [],
    totalCount: 0
}

export interface RepositoryActionsInterface {
    setRepositoryList: (data: RepositoryItemInterface[]) => void;
    setRepositoriesCount: (totalCount: number) => void;
    clearRepositoryList: () => void;
}

const setRepositoryList = (
    store: Store<RepositoryState, RepositoryActionsInterface>,
    data: RepositoryItemInterface[]
) => {
    store.setState({
        ...store.state,
        repositories: data
    });
}

const setRepositoriesCount = (
    store: Store<RepositoryState, RepositoryActionsInterface>,
    totalCount: number
) => {
    store.setState({
        ...store.state,
        totalCount
    });
}

const clearRepositoryList = (store: Store<RepositoryState, RepositoryActionsInterface>) => {
    store.setState({
        ...store.state,
        repositories: []
    });
}

export const RepositoryActions = {
    setRepositoryList,
    clearRepositoryList,
    setRepositoriesCount
}
