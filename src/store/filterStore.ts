import { Store } from "use-global-hook";

export interface FilterState {
    textFilter: string;
}

export const initialState: FilterState = {
    textFilter: ''
}

export interface FilterActionsInterface {
    setTextFilter: (value: string) => void;
}

const setTextFilter = (
    store: Store<FilterState, FilterActionsInterface>,
    value: string
) => {
    store.setState({
        ...store.state,
        textFilter: value
    });
}

export const FilterActions = {
    setTextFilter
}