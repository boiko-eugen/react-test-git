import globalHook from "use-global-hook";
import React from "react";
import * as repositoryStore from "./repositoryStore";
import * as filterStore from "./filterStore";

export interface GlobalState extends
    repositoryStore.RepositoryState,
    filterStore.FilterState
    {}

const globalInitialState: GlobalState = {
    ...repositoryStore.initialState,
    ...filterStore.initialState
}

export interface GlobalActions extends
    repositoryStore.RepositoryActionsInterface,
    filterStore.FilterActionsInterface
    {}

const globalActions = {
    ...repositoryStore.RepositoryActions,
    ...filterStore.FilterActions
}

export const useGlobal = globalHook<GlobalState, GlobalActions>(
    React,
    globalInitialState,
    globalActions
)